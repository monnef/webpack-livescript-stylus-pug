const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const fs = require('fs');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const templates = [];
const dir = 'src/pug';
const files = fs.readdirSync(dir);

files.forEach(file => {
    if (file.match(/\.pug$/)) {
        const filename = file.substring(0, file.length - 4);
        templates.push(
            new HtmlWebpackPlugin({
                template: dir + '/' + filename + '.pug',
                filename: filename + '.html',
                inject: true
            })
        );
    }
});

module.exports = {
    entry: {
        index: './src/index.ls'
    },
    devServer: {
        contentBase: './dist'
    },
    module: {
        rules: [
            {
                test: /\.pug$/,
                loader: 'pug-loader'
            },
            {
                test: /\.styl$/,
                loader: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'stylus-loader'
                ]
            },
            {
                test: /\.ls$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'livescript-plugin-loader',
                        options: {
                            // crashes
                            // plugins: [
                            //   {
                            //     name: 'livescript-transform-esm',
                            //     options: {format: 'esm'}
                            //   }
                            // ]
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        ...templates,
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    }
};
