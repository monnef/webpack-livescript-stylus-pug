webpack-livescript-stylus-pug 🎉
===

This project is a skeleton/scaffolding/template containing support for LiveScript (compiles to JavaScript), Stylus (compiles to CSS) and Pug (previously known as Jade, compiles to HTML).

All chosen technologies are powerful, expressive and have indentation-based syntax known from Python or Haskell.

💾 Installation
===

Download or clone repo:

```sh
git clone https://gitlab.com/monnef/webpack-livescript-stylus-pug.git
```

Go inside the new 📁directory:

```sh
cd webpack-livescript-stylus-pug
```

Install dependencies:

```sh
npm i
```

⌨️ Use
===


💭 Run dev mode with auto-reload
---

```sh
npm start
```

Open browser [http://localhost:8080](http://localhost:8080), your favorite editor or IDE (📁`/src`) and develop 🖍️.

🚀 Build for production
---

```sh
npm run build
```

Result is located at 📁`/dist`.

📜 License
===
**MIT**
